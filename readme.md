### Environment
Copy the .env.example file to .env and change the variables to your convenience
```sh
cp .env.example .env
```

### Create app_network
```sh
# Create a network, which allows containers to communicate
# with each other, by using their container name as a hostname
docker network create app_network
```

### Build
```sh
# Build prod using new BuildKit engine
COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose -f docker-compose.yml build
```

### Run
```bash
# Start prod in detached mode
#docker compose up -d
docker-compose -f docker-compose.yml up -d
```
